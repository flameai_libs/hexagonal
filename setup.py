from setuptools import setup


setup(
    name="common_hexagonal",
    description="My pesonal code for creating Hexagonal Driven Ports components implementation",
    version="v0.1.1",
    author="Alexander Andryukov",
    author_email="andryukov@gmail.com",
    extras_require={
        "dev": ["bump-my-version==0.20.0", "ruff==0.3.5"],
        "tests": ["pytest==8.1.1"],
    },
)
