from common.db.postgres.async_session import Session
from common.db.postgres.async_session import engine
from common.fastapi.base import AppBaseComponent
from common.fastapi.registry import ComponentCategoryEnum


class Postgres(AppBaseComponent):
    CATEGORY = ComponentCategoryEnum.RelationalDB

    session = Session

    async def shutdown(self) -> None:
        await engine.dispose()
