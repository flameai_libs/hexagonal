from typing import TYPE_CHECKING

from sqlalchemy.ext.asyncio import AsyncEngine
from sqlalchemy.ext.asyncio import create_async_engine

from common.hexagonal.base import RDB


class Postgres(RDB):
    def get_engine(self) -> AsyncEngine:
        return create_async_engine("postgresql+asyncpg://scott:tiger@localhost:5432/mydatabase")
