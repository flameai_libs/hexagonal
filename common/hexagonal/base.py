from abc import ABC
from abc import abstractmethod

from sqlalchemy.ext.asyncio import AsyncEngine
from sqlalchemy.ext.asyncio import AsyncSession

from common.hexagonal.registry import AdapterCategoryEnum
from common.hexagonal.registry import CategoryAdapter


class RDB(CategoryAdapter, ABC):
    category = AdapterCategoryEnum.RelationalDB

    @abstractmethod
    def get_session(self) -> AsyncSession: ...
    @abstractmethod
    def get_engine(self) -> AsyncEngine: ...
