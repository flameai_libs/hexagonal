"""
Здесь буду храниться настройки приложения
в гексагональных рамках в целом
"""

from __future__ import annotations


class App:
    @classmethod
    def register_adapter(cls) -> None:
        adapter.register()

    @classmethod
    def register_port(cls) -> None:
        pass
