# Библиотека для построения приложений с гексагональной архитектурой портов

## Пример использования:

requirements.txt

```text
--extra-index-url https://gitlab.com/api/v4/projects/flameai_libs%2Fpypi/packages/pypi/simple common-environ=={версия}
```

## Линтинг кода и код-стайл:

Установка для разработки

```sh
pip install -e ".[dev]"
```

Поднятие версии

```sh
bump-my-version bump {major/minor/patch} setup.py --tag --commit
```


Форматирование:

```sh
make format
```

Проверка:

```sh
make linting
```
